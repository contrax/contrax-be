# OAC is oapi-codegen

OAC_TAG = latest
OAC_IMG = registry.gitlab.com/contrax/oci-deepmap-oapi-codegen:$(OAC_TAG)
OAC_PATH_PREFIX = /local
OAC = docker pull $(OAC_IMG) && docker run --rm -u $(shell id -u ${USER}) -v ${PWD}:$(OAC_PATH_PREFIX) $(OAC_IMG)
OAC_PACKAGE = contraxapi
OAC_GEN_FILE = pkg/apigen/api.go
OAC_SPEC = api/openapi.yml

.PHONY: build
build:
	CGO_ENABLED=0 installsuffix=cgo govvv build -pkg gitlab.com/contrax/contrax-be/internal/version -ldflags "-extldflags '-static' -s" ./cmd/contrax

.PHONY: image
image: build
	docker build -f cmd/contrax/Dockerfile .

.PHONY: go-apigen
go-apigen:
	$(OAC) -package $(OAC_PACKAGE) -generate types,chi-server,spec,client $(OAC_PATH_PREFIX)/$(OAC_SPEC) > $(OAC_GEN_FILE)

.PHONY: fmt
fmt:
	gofumports -w $(shell find . -name "*.go" -not -path "*/\.*" | grep -v /pkg/apigen)

.PHONY: tokei
tokei:
	tokei $(shell find . -name "*.go" -not -path "*/\.*" | grep -v /pkg/apigen) $(shell find . -name "*.sql")

.PHONY: install-tools
install-tools:
	cd $(shell mktemp -d); go mod init tmp; go get mvdan.cc/gofumpt github.com/ahmetb/govvv
	cargo install tokei

.PHONY: lint
lint:
	go vet $(shell go list ./... | grep -v /pkg/apigen)

.PHONY: test
test:
	go test -race $(shell go list ./... | grep -v /pkg/apigen)
