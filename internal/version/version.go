package version

import (
	"strings"

	"github.com/rs/zerolog/log"
)

// Variables set at compile time.
var (
	GitBranch  string
	GitSummary string
	BuildDate  string
)

// Log the Contrax version information.
func Log() {
	log.Info().
		Str("gitBranch", GitBranch).
		Str("gitSummary", GitSummary).
		Str("buildDate", BuildDate).
		Msg("Logging Contrax version information")
}

func CreateSlug() string {
	var b strings.Builder

	b.WriteString(GitSummary)
	if GitBranch != "master" {
		b.WriteByte('-')
		b.WriteString(GitBranch)
	}
	b.WriteByte('-')
	b.WriteString(BuildDate)

	return b.String()
}
