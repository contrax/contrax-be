package repository

import (
	"context"

	"github.com/rs/zerolog"
	"gitlab.com/contrax/contrax-be/internal/cache"
	"gitlab.com/contrax/contrax-be/internal/database"
)

// Repository is an intermediate layer for retrieving data.
type Repository struct {
	log zerolog.Logger
	c   *cache.Cache
	d   *database.Wrapper
}

// NewRepository creates a new repository.
func NewRepository(log zerolog.Logger, c *cache.Cache, d *database.Wrapper) *Repository {
	return &Repository{log, c, d}
}

// GetUserByID retrieves a user from the cache or the database.
func (r *Repository) GetUserByID(ctx context.Context, id int64) (*database.User, error) {
	user, err := r.c.GetUserByID(ctx, id)
	if err == nil {
		r.log.Debug().Int64("id", id).Msg("Cache hit for user")
		return user, nil
	}

	user, err = r.d.GetUserByID(ctx, id)
	if err != nil {
		return nil, err
	}

	// Store the user in the cache.
	r.c.PutUser(user)

	return user, nil
}

// InvalidateCachedUser invalidates a cached user.
func (r *Repository) InvalidateCachedUser(ctx context.Context, u *database.User) error {
	return r.c.InvalidateUser(ctx, u)
}
