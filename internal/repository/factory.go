package repository

import (
	"flag"

	"gitlab.com/contrax/contrax-be/internal/cache"
	"gitlab.com/contrax/contrax-be/internal/database"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
)

// Factory is a factory for the repository.
type Factory struct {
	log zerolog.Logger
	c   *cache.Cache
	w   *database.Wrapper
}

// NewFactory creates a new repository factory.
func NewFactory() *Factory {
	return &Factory{
		log: zerolog.Nop(),
	}
}

// AddFlags adds command-line flags for the repository configuration options.
func (f *Factory) AddFlags(flagSet *flag.FlagSet) {}

// InitFromViper loads the repository configuration from Viper.
func (f *Factory) InitFromViper(v *viper.Viper) error {
	return nil
}

// Initialize initializes the factory.
func (f *Factory) Initialize(log zerolog.Logger, c *cache.Cache, w *database.Wrapper) {
	f.log = log
	f.c = c
	f.w = w
}

// CreateRepository creates the repository.
func (f *Factory) CreateRepository() *Repository {
	return NewRepository(f.log, f.c, f.w)
}
