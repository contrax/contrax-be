package tokenstore

import (
	"flag"
	"fmt"
	"time"

	"github.com/spf13/viper"
)

const (
	tokenStoreAddress         = "token-store.address"
	tokenStorePassword        = "token-store.password"
	tokenStoreDatabase        = "token-store.database"
	tokenStoreTokenExpiration = "token-store.token-expiration"
)

// Options contains the configuration options for the token store (Redis) connection.
type Options struct {
	Address  string
	Password string
	Database int

	TokenExpiration time.Duration
}

const defaultTokenExpiration = time.Hour * 24

// AddFlags adds the command-line flags for the database connection configuration.
func AddFlags(flagSet *flag.FlagSet) {
	flagSet.String(tokenStoreAddress, "", "Address of the Redis server")
	flagSet.String(tokenStorePassword, "", "Password for the Redis serer")
	flagSet.Int(tokenStoreDatabase, 0, "Database number for saving tokens in in the Redis server")
	flagSet.Duration(tokenStoreTokenExpiration, defaultTokenExpiration, "Amount of time before a token is set to expire")
}

// InitFromViper loads the database connection options from Viper.
func (opts *Options) InitFromViper(v *viper.Viper) (*Options, error) {
	address := v.GetString(tokenStoreAddress)
	password := v.GetString(tokenStorePassword)
	database := v.GetInt(tokenStoreDatabase)
	tokenExpiration := v.GetDuration(tokenStoreTokenExpiration)

	if address == "" {
		return opts, fmt.Errorf("%s is not set", tokenStoreAddress)
	}

	opts.Address = address
	opts.Password = password
	opts.Database = database
	opts.TokenExpiration = tokenExpiration

	return opts, nil
}
