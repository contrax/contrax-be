package tokenstore

import (
	"flag"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
)

// Factory is a factory for the token store.
type Factory struct {
	options *Options
	log     zerolog.Logger
}

// NewFactory creates a new token store factory.
func NewFactory() *Factory {
	return &Factory{
		options: &Options{},
		log:     zerolog.Nop(),
	}
}

// AddFlags adds command-line flags for the token store configuration options.
func (f *Factory) AddFlags(flagSet *flag.FlagSet) {
	AddFlags(flagSet)
}

// InitFromViper loads the token store configuration from Viper.
func (f *Factory) InitFromViper(v *viper.Viper) error {
	_, err := f.options.InitFromViper(v)
	return err
}

// Initialize initializes the factory.
func (f *Factory) Initialize(log zerolog.Logger) {
	f.log = log
}

// CreateTokenStore creates the token store.
func (f *Factory) CreateTokenStore() (*TokenStore, error) {
	return Connect(f.options, f.log)
}
