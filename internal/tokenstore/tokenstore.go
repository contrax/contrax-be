// Package tokenstore implements a wrapper for Redis used for saving tokens for users.
package tokenstore

import (
	"encoding/base64"
	"errors"
	"strings"
	"time"

	"github.com/go-redis/redis/v7"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"golang.org/x/crypto/sha3"
)

// TokenStore is an abstraction for storing tokens in Redis.
type TokenStore struct {
	cl   *redis.Client
	log  zerolog.Logger
	opts *Options
}

// Connect to Redis to create the token store.
func Connect(opts *Options, log zerolog.Logger) (*TokenStore, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     opts.Address,
		Password: opts.Password,
		DB:       opts.Database,
	})

	_, err := client.Ping().Result()
	if err != nil {
		return nil, errors.New("(tokenstore.Connect) could not ping Redis")
	}
	return &TokenStore{
		cl:   client,
		log:  log,
		opts: opts,
	}, nil
}

// CreateToken creates a new token for a user with the ID userID.
func (s *TokenStore) CreateToken(userID int64) (uuid.UUID, time.Duration, error) {
	token := uuid.New()
	key, err := tokenKey(token)
	if err != nil {
		return token, 0, err
	}

	expiresIn := s.opts.TokenExpiration
	return token, expiresIn, s.cl.Set(key, userID, expiresIn).Err()
}

// DeleteToken deletes a token.
func (s *TokenStore) DeleteToken(token uuid.UUID) error {
	key, err := tokenKey(token)
	if err != nil {
		return err
	}

	return s.cl.Del(key).Err()
}

// GetTokenUser retrieves the user associated with a token, and also returns the time till token expiration.
func (s *TokenStore) GetTokenUser(token uuid.UUID) (userID int64, expiresIn time.Duration, err error) {
	var key string
	key, err = tokenKey(token)
	if err != nil {
		return 0, 0, err
	}

	userID, err = s.cl.Get(key).Int64()
	if err != nil {
		return 0, 0, err
	}

	expiresIn, err = s.cl.TTL(key).Result()
	return
}

// tokenKey creates a new key from a token.
// Example: token:XyDXh86BCaKJUb5mK1miVtZsSBhZ2mu39hqSaBPNjDh66kLPcpSXE/5t9g4JbuAxhl1ZTpoCAzvpHESNGkbMRg:user.
func tokenKey(uid uuid.UUID) (string, error) {
	h := sha3.NewShake256()
	_, err := h.Write(uid[:])
	if err != nil {
		return "", err
	}

	hash := make([]byte, 64)
	_, err = h.Read(hash)
	if err != nil {
		return "", err
	}

	base64Hash := base64.RawStdEncoding.EncodeToString(hash)

	var b strings.Builder
	b.WriteString("token:")
	b.WriteString(base64Hash)
	b.WriteString(":user")

	return b.String(), nil
}
