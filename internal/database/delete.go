package database

import "context"

func (w *Wrapper) DeleteLoan(ctx context.Context, loanID int64) error {
	_, err := w.db.ExecContext(ctx, `DELETE FROM loan WHERE id = ?`, loanID)
	return err
}

func (w *Wrapper) DeleteLoanMutation(ctx context.Context, mutationID int64) error {
	_, err := w.db.ExecContext(ctx, `DELETE FROM pending_loan_mutation WHERE id = ?`, mutationID)
	return err
}
