package database

import (
	"flag"

	"github.com/rs/zerolog"

	"github.com/spf13/viper"
)

// Factory is a factory for a database wrapper.
type Factory struct {
	options *Options
	log     zerolog.Logger
}

// NewFactory creates a new database wrapper factory.
func NewFactory() *Factory {
	return &Factory{
		options: &Options{},
		log:     zerolog.Nop(),
	}
}

// AddFlags adds command-line flags for the database wrapper configuration options.
func (f *Factory) AddFlags(flagSet *flag.FlagSet) {
	AddFlags(flagSet)
}

// InitFromViper loads the database wrapper configuration from Viper.
func (f *Factory) InitFromViper(v *viper.Viper) error {
	_, err := f.options.InitFromViper(v)
	return err
}

// Initialize initializes the factory.
func (f *Factory) Initialize(log zerolog.Logger) {
	f.log = log
}

// CreateWrapper creates the database wrapper.
func (f *Factory) CreateWrapper() (*Wrapper, error) {
	return Connect(f.options, f.log)
}
