package database

import (
	"flag"
	"fmt"
	"os"

	"github.com/hashicorp/go-multierror"
	"github.com/spf13/viper"
)

const (
	databaseUser     = "database.user"
	databasePassword = "database.password"
	databaseAddress  = "database.address"
	databaseName     = "database.name"

	databaseMigrationsPath = "database.migrations-path"
)

// Options contains the configuration options for the database connection.
type Options struct {
	User     string
	Password string
	Address  string
	Name     string

	MigrationsPath string
}

// AddFlags adds the command-line flags for the database connection configuration.
func AddFlags(flagSet *flag.FlagSet) {
	flagSet.String(databaseUser, "", "User which should be logged in into the database with")
	flagSet.String(databasePassword, "", "Password for the user logged in with")
	flagSet.String(databaseAddress, "", "Address of the database")
	flagSet.String(databaseName, "", "The name of the database all data should be stored in")
	flagSet.String(databaseMigrationsPath, "migrations", "The path to the directory the migrations are in")
}

// InitFromViper loads the database connection options from Viper.
func (opts *Options) InitFromViper(v *viper.Viper) (*Options, error) {
	user := v.GetString(databaseUser)
	password := v.GetString(databasePassword)
	address := v.GetString(databaseAddress)
	name := v.GetString(databaseName)
	migrationsPath := v.GetString(databaseMigrationsPath)

	var err error
	if user == "" {
		err = multierror.Append(err, fmt.Errorf("%s is not set", databaseUser))
	}
	if password == "" {
		err = multierror.Append(err, fmt.Errorf("%s is not set", databasePassword))
	}
	if address == "" {
		err = multierror.Append(err, fmt.Errorf("%s is not set", databaseAddress))
	}
	if name == "" {
		err = multierror.Append(err, fmt.Errorf("%s is not set", databaseName))
	}
	if fInfo, statErr := os.Stat(migrationsPath); statErr != nil || !fInfo.IsDir() {
		err = multierror.Append(err, fmt.Errorf("the value of %s (%s) does not point to a valid directory",
			databaseMigrationsPath, migrationsPath,
		))
	}
	if err != nil {
		return opts, err
	}

	opts.User = user
	opts.Password = password
	opts.Address = address
	opts.Name = name
	opts.MigrationsPath = migrationsPath

	return opts, nil
}
