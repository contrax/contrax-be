package database

import (
	"bytes"
	"database/sql"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	"gitlab.com/contrax/contrax-be/pkg/auth"
)

// The database schema / migration version.
// See /migrations for migrations, which are always prefixed by their version number.
const version = 6

// Wrapper is the database wrapper / abstraction.
type Wrapper struct {
	db  *sqlx.DB
	log zerolog.Logger

	ph auth.PasswordHasher
}

// Auth is a struct representing a row in the auth table.
type Auth struct {
	UserID   int64
	PassHash []byte
	PassSalt []byte
}

// User is a struct representing a row in the user table.
type User struct {
	ID        int64
	Email     string
	FirstName string
	LastName  string
}

type ApprovalUser string

const (
	ApprovalUserFrom ApprovalUser = "from"
	ApprovalUserTo   ApprovalUser = "to"
)

// Loan is a struct representing a row in the loan table.
type Loan struct {
	ID           int64
	Amount       float64
	FromUserID   int64
	ToUserID     int64
	Description  string
	ApprovalUser ApprovalUser
	CreatedAt    time.Time
	FulfilledAt  sql.NullTime
	ApprovedAt   sql.NullTime
}

// PendingLoanMutation is a struct representing a row in the pending_loan_mutation.
type PendingLoanMutation struct {
	ID        int64
	LoanID    int64
	ByUserID  int64
	CreatedAt time.Time

	Amount      sql.NullFloat64
	Fulfilled   sql.NullTime
	Description sql.NullString
}

// NewAuth creates a new Auth for a password, and hashes their password with a newly generated salt.
// Note: does not set UserID.
func (w *Wrapper) NewAuth(password string) *Auth {
	hp := w.ph.NewHashedPassword(password)
	return &Auth{
		PassHash: hp.Hash[:],
		PassSalt: hp.Salt[:],
	}
}

// PasswordCorrect checks if password, hashed with the salt of the Auth, equals to the stored hash in the Auth.
func (w *Wrapper) PasswordCorrect(a *Auth, password string) bool {
	var hash [64]byte
	w.ph.Hash(password, a.PassSalt, hash[:])
	return bytes.Equal(hash[:], a.PassHash)
}
