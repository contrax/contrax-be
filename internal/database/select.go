package database

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/jmoiron/sqlx"

	squirrel "github.com/Masterminds/squirrel"
	contraxapi "gitlab.com/contrax/contrax-be/pkg/apigen"
)

type Order int

const (
	OrderDesc Order = iota
	OrderAsc
)

func (o Order) String() string {
	switch o {
	case OrderDesc:
		return "DESC"
	case OrderAsc:
		return "ASC"
	default:
		return ""
	}
}

type LoadedLoan struct {
	Loan

	FromUser *User `db:"-"`
	ToUser   *User `db:"-"`

	HasPendingLoanMutations bool `db:"-"`
}

type LoadedLoanMutation struct {
	PendingLoanMutation
	ByUser *User       `db:"-"`
	Loan   *LoadedLoan `db:"-"`
}

type Balance struct {
	Debt   float64
	Credit float64
}

type GetLoansFilter struct {
	WithUserID *int64
	Fulfilled  *bool
	Approved   *bool
}

func (f GetLoansFilter) Apply(query squirrel.SelectBuilder) squirrel.SelectBuilder {
	if f.Fulfilled != nil {
		var pred interface{}
		if *f.Fulfilled {
			pred = squirrel.NotEq{"fulfilled_at": nil}
		} else {
			pred = squirrel.Eq{"fulfilled_at": nil}
		}
		query = query.Where(pred)
	}
	if f.Approved != nil {
		var pred interface{}
		if *f.Approved {
			pred = squirrel.NotEq{"approved_at": nil}
		} else {
			pred = squirrel.Eq{"approved_at": nil}
		}
		query = query.Where(pred)
	}
	if f.WithUserID != nil {
		query = query.Where(squirrel.Or{
			squirrel.Eq{"from_user_id": *f.WithUserID},
			squirrel.Eq{"to_user_id": *f.WithUserID},
		})
	}
	return query
}

type GetLoansParams struct {
	Cursor  interface{}
	Limit   int64
	Order   Order
	OrderBy string
	Filter  GetLoansFilter
}

type GetMutationsParams struct {
	Cursor interface{}
	Limit  int64
	Order  Order
}

func orderFromString(s string) (Order, error) {
	var order Order
	switch s {
	case "desc":
		order = OrderDesc
	case "asc":
		order = OrderAsc
	default:
		return 0, errors.New("invalid order")
	}
	return order, nil
}

func GetLoansParamsFromAPI(params *contraxapi.GetLoansParams) (*GetLoansParams, error) {
	// Default values or set by the user.
	cursorString := orString(params.Cursor, "")
	limit := orInt64(params.Limit, 50)
	orderString := orString(params.Order, "desc")
	orderByString := orString(params.OrderBy, "createdAt")

	// Read the order.
	order, err := orderFromString(orderString)
	if err != nil {
		return nil, err
	}

	// Validate orderBy, parse the cursor.
	var prop string
	var cursor interface{}
	switch orderByString {
	case "createdAt":
		prop = "created_at"
		if cursorString != "" {
			cursor, err = time.Parse(time.RFC3339, cursorString)
		}
	case "amount":
		prop = "amount"
		if cursorString != "" {
			cursor, err = strconv.ParseFloat(cursorString, 64)
		}
	default:
		err = errors.New("invalid orderBy property")
	}
	if err != nil {
		return nil, err
	}

	return &GetLoansParams{
		Cursor:  cursor,
		Limit:   limit,
		Order:   order,
		OrderBy: prop,
		Filter: GetLoansFilter{
			WithUserID: params.FilterWithUserId,
			Approved:   params.FilterApproved,
			Fulfilled:  params.FilterFulfilled,
		},
	}, nil
}

func GetMutationsParamsFromAPI(params *contraxapi.GetLoanMutationsParams) (*GetMutationsParams, error) {
	// Default values or set by the user.
	cursorString := orString(params.Cursor, "")
	limit := orInt64(params.Limit, 50)
	orderString := orString(params.Order, "desc")

	// Read the order.
	order, err := orderFromString(orderString)
	if err != nil {
		return nil, err
	}

	// Parse the cursor.
	var cursor interface{}
	if cursorString != "" {
		cursor, err = time.Parse(time.RFC3339, cursorString)
	}
	if err != nil {
		return nil, err
	}

	return &GetMutationsParams{
		Cursor: cursor,
		Limit:  limit,
		Order:  order,
	}, nil
}

func (w *Wrapper) GetLoans(ctx context.Context, userID int64, params *GetLoansParams) ([]LoadedLoan, error) {
	loans, err := w.getLoans(ctx, userID, params)
	if err != nil {
		return nil, err
	}
	return loans, w.loadLoans(ctx, userID, loans)
}

func (w *Wrapper) getLoans(ctx context.Context, userID int64, params *GetLoansParams) ([]LoadedLoan, error) {
	query := squirrel.
		Select("*").
		From("loan").
		Where(squirrel.Or{
			squirrel.Eq{"from_user_id": userID},
			squirrel.Eq{"to_user_id": userID},
		})
	if params.Cursor != nil {
		var pred interface{}
		switch params.Order {
		case OrderDesc:
			pred = squirrel.Lt{params.OrderBy: params.Cursor}
		case OrderAsc:
			pred = squirrel.Gt{params.OrderBy: params.Cursor}
		}
		query = query.Where(pred)
	}

	query = params.Filter.Apply(query)

	queryStr, args, err := query.
		OrderByClause("? "+params.Order.String(), params.OrderBy).
		ToSql()
	if err != nil {
		return nil, fmt.Errorf("could not create query: %w", err)
	}

	loadedLoans := make([]LoadedLoan, 0)
	return loadedLoans, w.db.SelectContext(ctx, &loadedLoans, queryStr, args...)
}

func (w *Wrapper) loadLoans(ctx context.Context, userID int64, loans []LoadedLoan) error {
	if len(loans) == 0 {
		return nil
	}

	err := w.loadLoanUsers(ctx, userID, loans)
	if err != nil {
		return err
	}
	return w.loadLoanPendingMutations(ctx, loans)
}

func (w *Wrapper) loadLoanUsers(ctx context.Context, userID int64, loans []LoadedLoan) error {
	mapping := map[int64][]*LoadedLoan{
		userID: make([]*LoadedLoan, 0, len(loans)),
	}
	for i, loan := range loans {
		mapping[loan.FromUserID] = append(mapping[loan.FromUserID], &loans[i])
		mapping[loan.ToUserID] = append(mapping[loan.ToUserID], &loans[i])
	}

	ids := make([]int64, 0, len(mapping))
	for id := range mapping {
		ids = append(ids, id)
	}

	queryStr, args, err := squirrel.
		Select("*").
		From("user").
		Where(squirrel.Eq{"id": ids}).
		ToSql()
	if err != nil {
		return err
	}

	users := make([]User, len(ids))
	err = w.db.SelectContext(ctx, &users, queryStr, args...)
	if err != nil {
		return err
	}

	for i := range users {
		user := &users[i]
		for _, loan := range mapping[user.ID] {
			if loan.FromUserID == user.ID {
				loan.FromUser = user
			} else if loan.ToUserID == user.ID {
				loan.ToUser = user
			}
		}
	}

	return nil
}

func (w *Wrapper) loadLoanPendingMutations(ctx context.Context, loans []LoadedLoan) error {
	mapping := make(map[int64]*LoadedLoan, len(loans))
	for i := range loans {
		mapping[loans[i].ID] = &loans[i]
	}

	loanIDs := make([]int64, 0, len(loans))
	for _, loan := range loans {
		loanIDs = append(loanIDs, loan.ID)
	}

	query, args, err := sqlx.In(`
		SELECT loan_id, COUNT(*) FROM pending_loan_mutation 
		WHERE loan_id IN (?)
		GROUP BY loan_id
	`, loanIDs)
	if err != nil {
		return err
	}

	rows, err := w.db.QueryContext(ctx, query, args...)
	if err != nil {
		return err
	}

	for rows.Next() {
		var loanID int64
		var amount int64
		err = rows.Scan(&loanID, &amount)
		if err != nil {
			return err
		}

		mapping[loanID].HasPendingLoanMutations = amount > 0
	}

	return rows.Close()
}

// GetUserByEmail retrieves a User by their email address.
func (w *Wrapper) GetUserByEmail(ctx context.Context, email string) (*User, error) {
	var user User
	err := w.db.GetContext(ctx, &user, `SELECT * FROM user WHERE email = ?`, email)
	return &user, err
}

// GetUserByID retrieves a User by their ID.
func (w *Wrapper) GetUserByID(ctx context.Context, id int64) (*User, error) {
	var user User
	err := w.db.GetContext(ctx, &user, `SELECT * FROM user WHERE id = ?`, id)
	return &user, err
}

// GetBalance retrieves the balance for a user.
func (w *Wrapper) GetBalance(ctx context.Context, userID int64) (*Balance, error) {
	var debt sql.NullFloat64
	err := w.db.GetContext(ctx, &debt, `
		SELECT SUM(amount) FROM loan 
		WHERE to_user_id = ?
		AND approved_at IS NOT NULL
		AND fulfilled_at IS NULL
	`, userID)
	if err != nil {
		return nil, err
	}

	var credit sql.NullFloat64
	err = w.db.GetContext(ctx, &credit, `
		SELECT SUM(amount) FROM loan 
		WHERE from_user_id = ?
		AND approved_at IS NOT NULL
		AND fulfilled_at IS NULL
	`, userID)
	if err != nil {
		return nil, err
	}

	return &Balance{
		Credit: credit.Float64,
		Debt:   debt.Float64,
	}, nil
}

func (w *Wrapper) IsUserAuthorizedToReadLoan(ctx context.Context, userID, loanID int64) (bool, error) {
	const q = `SELECT COUNT(*) FROM loan WHERE (from_user_id = ? OR to_user_id = ?) AND id = ?`

	var count int
	err := w.db.GetContext(ctx, &count, q, userID, userID, loanID)
	if err != nil {
		return false, err
	}

	return count > 0, nil
}

// GetLoan retrieves a single loan.
func (w *Wrapper) GetLoan(ctx context.Context, loanID int64) (*LoadedLoan, error) {
	var loan LoadedLoan
	err := w.db.GetContext(ctx, &loan, `SELECT * FROM loan WHERE id = ?`, loanID)
	if err != nil {
		return nil, err
	}

	err = w.loadLoan(ctx, &loan)
	if err != nil {
		return nil, err
	}

	return &loan, nil
}

func (w *Wrapper) GetMutation(ctx context.Context, mutationID int64) (*LoadedLoanMutation, error) {
	var mut LoadedLoanMutation
	err := w.db.GetContext(ctx, &mut, `SELECT * FROM pending_loan_mutation WHERE id = ?`, mutationID)
	if err != nil {
		return nil, err
	}

	err = w.loadMutation(ctx, &mut)
	if err != nil {
		return nil, err
	}

	return &mut, nil
}

func (w *Wrapper) GetMutations(ctx context.Context,
	loanID int64, params *GetMutationsParams,
) ([]LoadedLoanMutation, error) {
	query := squirrel.Select("*").
		From("pending_loan_mutation").
		Where(squirrel.Eq{"loan_id": loanID})

	if params.Cursor != nil {
		var pred interface{}
		switch params.Order {
		case OrderDesc:
			pred = squirrel.Lt{"created_at": params.Cursor}
		case OrderAsc:
			pred = squirrel.Gt{"created_at": params.Cursor}
		}
		query = query.Where(pred)
	}

	queryStr, args, err := query.
		OrderBy("created_at " + params.Order.String()).
		ToSql()
	if err != nil {
		return nil, err
	}

	var mutations []LoadedLoanMutation
	err = w.db.SelectContext(ctx, &mutations, queryStr, args...)
	if err != nil {
		return nil, err
	}

	err = w.loadMutations(ctx, loanID, mutations)
	if err != nil {
		return nil, err
	}

	return mutations, nil
}

func (w *Wrapper) loadMutation(ctx context.Context, mut *LoadedLoanMutation) error {
	loan, err := w.GetLoan(ctx, mut.LoanID)
	if err != nil {
		return err
	}

	user, err := w.GetUserByID(ctx, mut.ByUserID)
	if err != nil {
		return err
	}

	mut.Loan = loan
	mut.ByUser = user
	if mut.Loan.FromUser.ID == mut.ByUserID {
		mut.ByUser = mut.Loan.FromUser
	} else if mut.Loan.ToUser.ID == mut.ByUserID {
		mut.ByUser = mut.Loan.ToUser
	}

	return nil
}

func (w *Wrapper) loadMutations(ctx context.Context, loanID int64, mutations []LoadedLoanMutation) error {
	const q = `
		SELECT u.* FROM user u 
		INNER JOIN pending_loan_mutation m ON m.by_user_id = u.id
		WHERE m.loan_id = ?
	`

	var users []User
	err := w.db.SelectContext(ctx, &users, q, loanID)
	if err != nil {
		return err
	}

	m := make(map[int64][]*LoadedLoanMutation, 2)
	for i := range mutations {
		m[mutations[i].ByUserID] = append(m[mutations[i].ByUserID], &mutations[i])
	}

	for i := range users {
		for _, mut := range m[users[i].ID] {
			mut.ByUser = &users[i]
		}
	}

	return nil
}

func (w *Wrapper) loadLoan(ctx context.Context, loan *LoadedLoan) error {
	var users []User

	query, args, err := sqlx.In(`SELECT * FROM user WHERE id IN (?)`,
		[]int64{loan.FromUserID, loan.ToUserID},
	)
	if err != nil {
		return err
	}

	err = w.db.SelectContext(ctx, &users, query, args...)
	if err != nil {
		return err
	}

	for i := range users {
		user := &users[i]

		if loan.FromUserID == user.ID {
			loan.FromUser = user
		} else if loan.ToUserID == user.ID {
			loan.ToUser = user
		}
	}

	loan.HasPendingLoanMutations, err = w.HasPendingLoanMutations(ctx, loan.ID)
	return err
}

func (w *Wrapper) HasPendingLoanMutations(ctx context.Context, loanID int64) (bool, error) {
	var amount int
	err := w.db.GetContext(ctx, &amount, `SELECT COUNT(*) FROM pending_loan_mutation WHERE loan_id = ?`, loanID)
	return amount > 0, err
}

func orString(s *string, or string) string {
	if s == nil {
		return or
	}
	return *s
}

func orInt64(i *int64, or int64) int64 {
	if i == nil {
		return or
	}
	return *i
}
