package database

import (
	"errors"
	"fmt"
	"strings"
	"unicode"

	"gitlab.com/contrax/contrax-be/pkg/auth"

	"github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"
)

// Connect creates a new database wrapper and migrates the database to the configured version.
func Connect(options *Options, log zerolog.Logger) (*Wrapper, error) {
	// Use Zerolog for the MySQL driver.
	err := mysql.SetLogger(&log)
	if err != nil {
		return nil, fmt.Errorf("could not set logger: %w", err)
	}

	err = migrateDB(options, log)
	if err != nil {
		return nil, fmt.Errorf("could not migrate database: %w", err)
	}

	// Create the DSN and connect to the database.
	dsn := createDSN(options)
	db, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		return nil, fmt.Errorf("could not connect to database: %w", err)
	}
	err = db.Ping()
	if err != nil {
		return nil, errors.New("could not ping database")
	}

	// Add the name mapper, which maps struct field names to column names.
	db.MapperFunc(nameMapper)

	w := Wrapper{
		db:  db,
		log: log,
		ph:  auth.NewPasswordHasher(log),
	}
	return &w, nil
}

// nameMapper maps Golang struct field names to table columns.
// For example, ID would be mapped to id and UserID would be mapped to user_id.
func nameMapper(s string) string {
	prevUpper := true // we start with true, so we don't get an underscore before every name.

	var b strings.Builder
	for _, r := range s {
		if unicode.IsUpper(r) {
			if !prevUpper {
				b.WriteByte('_')
				prevUpper = true
			}
		} else {
			prevUpper = false
		}
		b.WriteRune(unicode.ToLower(r))
	}

	return b.String()
}

// CreateDSN creates a MySQL DSN (Data Source Name), used for connecting to MySQL.
func createDSN(options *Options, additionalParams ...map[string]string) string {
	params := map[string]string{
		"parseTime": "true",
		"collation": "utf8mb4_unicode_ci",
	}
	for _, p := range additionalParams {
		for k, v := range p {
			params[k] = v
		}
	}

	return (&mysql.Config{
		AllowNativePasswords: true,

		Net:    "tcp",
		User:   options.User,
		Passwd: options.Password,
		Addr:   options.Address,
		DBName: options.Name,
		Params: params,
	}).FormatDSN()
}
