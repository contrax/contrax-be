package database

import (
	"context"
	"time"

	"github.com/Masterminds/squirrel"
)

func (w *Wrapper) UpdateLoan(ctx context.Context, l *Loan) (*LoadedLoan, error) {
	q := `UPDATE loan SET amount = ?, approved_at = ?, fulfilled_at = ? WHERE id = ?`

	_, err := w.db.ExecContext(ctx, q,
		l.Amount, l.ApprovedAt, l.FulfilledAt, l.ID,
	)
	if err != nil {
		return nil, err
	}

	return w.GetLoan(ctx, l.ID)
}

func (w *Wrapper) ApproveLoan(ctx context.Context, loanID int64) error {
	_, err := w.db.ExecContext(ctx, `UPDATE loan SET approved_at = ? WHERE id = ?`, time.Now(), loanID)
	return err
}

func (w *Wrapper) RejectLoan(ctx context.Context, loanID int64) error {
	return w.DeleteLoan(ctx, loanID)
}

func (w *Wrapper) AcceptLoanMutation(ctx context.Context, mutationID int64) error {
	mutation, err := w.GetMutation(ctx, mutationID)
	if err != nil {
		return err
	}

	query := squirrel.Update("loan").Where(squirrel.Eq{"id": mutation.LoanID})
	if mutation.Amount.Valid {
		query = query.Set("amount", mutation.Amount.Float64)
	}
	if mutation.Fulfilled.Valid {
		query = query.Set("fulfilled_at", mutation.Fulfilled.Time)
	}
	if mutation.Description.Valid {
		query = query.Set("description", mutation.Description.String)
	}

	queryString, args, err := query.ToSql()
	if err != nil {
		return err
	}

	_, err = w.db.ExecContext(ctx, queryString, args...)
	if err != nil {
		return err
	}

	return w.DeleteLoanMutation(ctx, mutationID)
}

func (w *Wrapper) RejectLoanMutation(ctx context.Context, mutationID int64) error {
	return w.DeleteLoanMutation(ctx, mutationID)
}
