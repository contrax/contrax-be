package database

import (
	"database/sql"
	"fmt"

	"github.com/rs/zerolog/log"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/mysql"
	"github.com/rs/zerolog"

	// Import the MySQL / MariaDB driver.
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

// migrateDB migrates the database.
func migrateDB(options *Options, log zerolog.Logger) error {
	err := maybeCreateDB(options)
	if err != nil {
		return fmt.Errorf("could not optionally create the database: %w", err)
	}

	// MultiStatements is not enabled by default, but we assume the migrations are safe,
	// and it would not be practical to have multiStatements turned off for migrations.
	db, err := sql.Open("mysql", createDSN(options, map[string]string{
		"multiStatements": "true",
	}))
	if err != nil {
		return fmt.Errorf("could not open database: %w", err)
	}
	defer func() {
		// Attempt to close the database after the function returns or panics.
		err = db.Close()
		if err != nil {
			log.Error().Err(err).Msg("Error closing database after migration")
		}
	}()

	// Create the migration driver.
	driver, err := mysql.WithInstance(db, &mysql.Config{
		MigrationsTable: "migrations",
		DatabaseName:    options.Name,
	})
	if err != nil {
		return fmt.Errorf("could not create migration driver: %w", err)
	}

	// Create the Migrate instance, using files in the directory 'migrations' as a source.
	m, err := migrate.NewWithDatabaseInstance(
		"file://"+options.MigrationsPath,
		options.Name, driver)
	if err != nil {
		return fmt.Errorf("could not create migration instance: %w", err)
	}

	// Check the migration version.
	ver, dirty, err := m.Version()
	if err != nil {
		log.Warn().Msg("Could not retrieve migration version, asserting new database")
	}

	// If the migration has not yet completely finished (dirty), or not yet done, run it.
	if err != nil || dirty || ver != version {
		err := m.Migrate(version)
		if err != nil {
			return fmt.Errorf("could not migrate: %w", err)
		}
	}
	return nil
}

// maybeCreateDB creates the `contrax` database if it does not exist already.
func maybeCreateDB(options *Options) error {
	newOptions := *options
	newOptions.Name = ""

	db, err := sql.Open("mysql", createDSN(&newOptions))
	if err != nil {
		return err
	}
	defer func() {
		// Attempt to close the database after the function returns or panics.
		err = db.Close()
		if err != nil {
			log.Error().Err(err).Msg("Error closing database after optional creation")
		}
	}()

	_, err = db.Exec(`CREATE DATABASE IF NOT EXISTS ` + options.Name + ` COLLATE utf8mb4_unicode_ci`)
	return err
}
