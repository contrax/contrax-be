package database

import (
	"context"
	"fmt"
	"time"
)

// CheckAuth checks if password is the password for a user with ID userID.
func (w *Wrapper) CheckAuth(ctx context.Context, userID int64, password string) (bool, error) {
	var authInfo Auth
	err := w.db.GetContext(ctx, &authInfo, `SELECT * FROM auth WHERE user_id = ?`,
		userID,
	)
	if err != nil {
		return false, err
	}
	return w.PasswordCorrect(&authInfo, password), nil
}

// CreateUser creates a new User in the database, returning the User with its new ID.
func (w *Wrapper) CreateUserAndAuth(ctx context.Context, user *User, auth *Auth) (*User, error) {
	tx, err := w.db.Beginx()
	if err != nil {
		return nil, err
	}

	_, err = tx.ExecContext(ctx, `INSERT INTO user (email, first_name, last_name) VALUES (?, ?, ?)`,
		user.Email, user.FirstName, user.LastName,
	)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return nil, fmt.Errorf(
				"user create failed: %w, after that, the the rollback failed: %v",
				err, rollbackErr,
			)
		}
		return nil, err
	}

	// Retrieve the ID of the newly created User.
	u := *user
	err = tx.GetContext(ctx, &u.ID, `SELECT LAST_INSERT_ID()`)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return nil, fmt.Errorf(
				"user create (insert ID read) failed: %w, after that, the the rollback failed: %v",
				err, rollbackErr,
			)
		}
		return nil, err
	}

	auth.UserID = u.ID
	_, err = tx.ExecContext(ctx, `INSERT INTO auth (user_id, pass_hash, pass_salt) VALUES (?, ?, ?)`,
		auth.UserID, auth.PassHash, auth.PassSalt,
	)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return nil, fmt.Errorf(
				"user create (insert auth) failed: %w, after that, the the rollback failed: %v",
				err, rollbackErr,
			)
		}
		return nil, err
	}

	return &u, tx.Commit()
}

// CreateLoan creates a new Loan in the database, returning it with its new ID.
func (w *Wrapper) CreateLoan(ctx context.Context, loan *Loan) (*LoadedLoan, error) {
	q := `
		INSERT INTO loan (amount, from_user_id, to_user_id, approval_user, created_at, description)
		VALUES (?, ?, ?, ?, ?, ?)
	`

	loan.CreatedAt = time.Now()
	_, err := w.db.ExecContext(ctx, q,
		loan.Amount, loan.FromUserID, loan.ToUserID, loan.ApprovalUser, loan.CreatedAt, loan.Description,
	)
	if err != nil {
		return nil, err
	}

	// Retrieve the ID of the newly created loan.
	l := *loan
	err = w.db.GetContext(ctx, &l.ID, `SELECT LAST_INSERT_ID()`)
	if err != nil {
		return nil, err
	}

	newLoan := LoadedLoan{Loan: l}
	err = w.loadLoan(ctx, &newLoan)
	if err != nil {
		return nil, err
	}

	return &newLoan, nil
}

// CreateMutation creates a new LoanMutation in the database, returning it with its new ID.
func (w *Wrapper) CreateMutation(ctx context.Context, mut *PendingLoanMutation) (*LoadedLoanMutation, error) {
	q := `
		INSERT INTO pending_loan_mutation (loan_id, by_user_id, created_at, amount, fulfilled, description) 
		VALUES (?, ?, ?, ?, ?, ?)
	`

	mut.CreatedAt = time.Now()
	_, err := w.db.ExecContext(ctx, q,
		mut.LoanID, mut.ByUserID, mut.CreatedAt, mut.Amount, mut.Fulfilled, mut.Description,
	)
	if err != nil {
		return nil, err
	}

	// Retrieve the ID of the newly created mutation.
	m := *mut
	err = w.db.GetContext(ctx, &m.ID, `SELECT LAST_INSERT_ID()`)
	if err != nil {
		return nil, err
	}

	newMutation := LoadedLoanMutation{PendingLoanMutation: m}
	err = w.loadMutation(ctx, &newMutation)
	if err != nil {
		return nil, err
	}

	return &newMutation, nil
}
