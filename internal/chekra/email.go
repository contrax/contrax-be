package chekra

import mailchecker "github.com/FGRibreau/mailchecker/v3/platform/go"

// Email checks the validity of an email address.
func Email(email string) bool {
	return mailchecker.IsValid(email)
}
