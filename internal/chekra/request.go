package chekra

import (
	"errors"
	"fmt"
	"strings"

	contraxapi "gitlab.com/contrax/contrax-be/pkg/apigen"
)

const loanAmountMinimum = 0.01

// CreateAccountRequest checks the validity of a createAccount request.
func CreateAccountRequest(account *contraxapi.CreateAccountJSONRequestBody) error {
	// If the email address contains a colon, basic auth will be broken.
	if strings.ContainsRune(account.Email, ':') {
		return errors.New("email may not contain a colon")
	}

	// Check if the email address is valid.
	if !Email(account.Email) {
		return errors.New("invalid email address")
	}

	// Check if firstName is valid.
	if account.FirstName == "" {
		return errors.New("firstName must not be empty")
	}

	// Check if lastName is valid.
	if account.LastName == "" {
		return errors.New("lastName must not be empty")
	}

	// Check if the password is not empty.
	if account.Password == "" {
		return errors.New("password must not be empty")
	}

	return nil
}

// CreateLoanRequest checks the validity of a createLoan request.
func CreateLoanRequest(loan *contraxapi.CreateLoanJSONRequestBody) error {
	if loan.Amount < loanAmountMinimum {
		return fmt.Errorf("amount under minimum (%f)", loanAmountMinimum)
	}

	if loan.ReceivingUser != "other" && loan.ReceivingUser != "current" {
		return errors.New("receivingUser has an invalid value")
	}

	return nil
}

// CreateLoanMutationRequest checks the validity of a createMutation request.
func CreateLoanMutationRequest(mut *contraxapi.CreateLoanMutationRequest) error {
	if mut.Amount != nil && *mut.Amount < 0.01 {
		return errors.New("amount under minimum (0.01)")
	}
	return nil
}
