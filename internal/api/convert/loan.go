package convert

import (
	"database/sql"
	"time"

	"github.com/AlekSi/pointer"

	"gitlab.com/contrax/contrax-be/internal/database"
	contraxapi "gitlab.com/contrax/contrax-be/pkg/apigen"
)

// LoadedLoansToAPI converts one or more database.LoadedLoans to a contraxapi.Loans object.
func LoadedLoansToAPI(loans ...database.LoadedLoan) contraxapi.Loans {
	apiLoans := make([]contraxapi.Loan, 0, len(loans))
	for i := range loans {
		apiLoans = append(apiLoans, LoadedLoanToAPI(&loans[i]))
	}

	return contraxapi.Loans{Data: &apiLoans}
}

// LoadedLoanToAPI converts a *database.LoadedLoan to a contraxapi.Loan.
func LoadedLoanToAPI(l *database.LoadedLoan) contraxapi.Loan {
	loan := contraxapi.Loan{
		Id:                  l.ID,
		Amount:              l.Amount,
		CreatedAt:           l.CreatedAt,
		FulfilledAt:         pointerToNullTimeOrNil(&l.FulfilledAt),
		ApprovedAt:          pointerToNullTimeOrNil(&l.ApprovedAt),
		Description:         pointer.ToStringOrNil(l.Description),
		ApprovalUser:        string(l.ApprovalUser),
		HasPendingMutations: l.HasPendingLoanMutations,
	}

	loan.FromUser.User = UserToAPI(l.FromUser)
	loan.ToUser.User = UserToAPI(l.ToUser)

	return loan
}

// UserToAPI converts a *database.User to a contraxapi.User.
func UserToAPI(u *database.User) contraxapi.User {
	if u == nil {
		return contraxapi.User{}
	}

	return contraxapi.User{
		Id:        u.ID,
		Email:     u.Email,
		FirstName: u.FirstName,
		LastName:  u.LastName,
	}
}

// pointerToNullTimeOrNil takes a sql.NullTime and returns a pointer to its (copied) value if it is
//
// 1) Valid
//
// 2) Not zero (time.Time{} is)
//
// Otherwise, it returns nil.
func pointerToNullTimeOrNil(t *sql.NullTime) *time.Time {
	if t.Valid {
		return pointer.ToTimeOrNil(t.Time)
	}
	return nil
}

// pointerToNullFloat64OrNil takes a sql.NullFloat64 and returns a pointer to its (copied) value if it is valid.
// Otherwise, it returns nil.
func pointerToNullFloat64OrNil(f *sql.NullFloat64) *float64 {
	if f.Valid {
		return pointer.ToFloat64(f.Float64)
	}
	return nil
}

// pointerToNullStringOrNil takes a sql.NullString and returns a pointer to its (copied) value if it is valid.
// Otherwise, it returns nil.
func pointerToNullStringOrNil(s *sql.NullString) *string {
	if s.Valid {
		return pointer.ToString(s.String)
	}
	return nil
}
