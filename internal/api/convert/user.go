package convert

import (
	"strconv"

	"github.com/AlekSi/pointer"

	"gitlab.com/contrax/contrax-be/internal/database"
	contraxapi "gitlab.com/contrax/contrax-be/pkg/apigen"
)

// UsersToAPI converts one or more database.Users to a *contraxapi.Users object.
func UsersToAPI(users ...database.User) *contraxapi.Users {
	data := make([]contraxapi.User, len(users))
	for i := range users {
		data[i] = UserToAPI(&users[i])
	}

	var cursor string
	if len(data) > 0 {
		cursor = strconv.FormatInt(data[len(data)-1].Id, 10)
	}

	return &contraxapi.Users{
		Data:       &data,
		NextCursor: pointer.ToStringOrNil(cursor),
	}
}
