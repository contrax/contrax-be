package convert

import (
	"database/sql"
	"time"

	"gitlab.com/contrax/contrax-be/internal/database"
	contraxapi "gitlab.com/contrax/contrax-be/pkg/apigen"
)

// CreateLoanMutationRequestFromAPI converts a contraxapi.CreateLoanMutationRequest
// to a database.PendingLoanMutation.
func CreateLoanMutationRequestFromAPI(m *contraxapi.CreateLoanMutationRequest) database.PendingLoanMutation {
	mutation := database.PendingLoanMutation{
		Amount:      nullFloat64FromPointer(m.Amount),
		Description: nullStringFromPointer(m.Description),
	}
	if m.Fulfilled != nil && *m.Fulfilled {
		mutation.Fulfilled = sql.NullTime{
			Valid: true,
			Time:  time.Now(),
		}
	}
	return mutation
}

// LoadedMutationToAPI converts a *database.LoadedLoanMutation to a contraxapi.LoanMutation.
func LoadedMutationToAPI(m *database.LoadedLoanMutation) contraxapi.LoanMutation {
	mutation := contraxapi.LoanMutation{
		Id:          m.ID,
		Amount:      pointerToNullFloat64OrNil(&m.Amount),
		Description: pointerToNullStringOrNil(&m.Description),
		Fulfilled:   pointerToNullTimeOrNil(&m.Fulfilled),
		CreatedAt:   m.CreatedAt,
	}

	mutation.ByUser.User = UserToAPI(m.ByUser)
	if m.Loan != nil {
		mutation.Loan = &struct{ contraxapi.Loan }{LoadedLoanToAPI(m.Loan)}
	}

	return mutation
}

func LoadedMutationsToAPI(mutations []database.LoadedLoanMutation) contraxapi.LoanMutations {
	apiMutations := make([]contraxapi.LoanMutation, len(mutations))
	for i := range mutations {
		apiMutations[i] = LoadedMutationToAPI(&mutations[i])
	}

	return contraxapi.LoanMutations{
		Data: &apiMutations,
	}
}

func nullFloat64FromPointer(f *float64) sql.NullFloat64 {
	if f == nil {
		return sql.NullFloat64{Valid: false}
	}
	return sql.NullFloat64{Float64: *f, Valid: true}
}

func nullStringFromPointer(s *string) sql.NullString {
	if s == nil {
		return sql.NullString{Valid: false}
	}
	return sql.NullString{String: *s, Valid: true}
}
