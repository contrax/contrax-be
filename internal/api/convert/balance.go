package convert

import (
	"gitlab.com/contrax/contrax-be/internal/database"
	contraxapi "gitlab.com/contrax/contrax-be/pkg/apigen"
)

// BalanceToAPI converts a *database.Balance to a *contraxapi.Balance.
func BalanceToAPI(b *database.Balance) *contraxapi.Balance {
	return &contraxapi.Balance{
		Credit: b.Credit,
		Debt:   b.Debt,
	}
}
