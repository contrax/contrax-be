package api

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/contrax/contrax-be/internal/api/convert"
	"gitlab.com/contrax/contrax-be/internal/chekra"
	"gitlab.com/contrax/contrax-be/internal/database"
	"gitlab.com/contrax/contrax-be/internal/repository"
	"gitlab.com/contrax/contrax-be/internal/tokenstore"
	"gitlab.com/contrax/contrax-be/internal/version"
	contraxapi "gitlab.com/contrax/contrax-be/pkg/apigen"

	"github.com/AlekSi/pointer"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"
	"github.com/rs/zerolog/log"
)

// Server is the API server.
type Server struct {
	options Options
	log     zerolog.Logger
	repo    *repository.Repository
	db      *database.Wrapper
	ts      *tokenstore.TokenStore
}

// ListenAndServe starts the API server.
func (s *Server) ListenAndServe() error {
	r := chi.NewMux()
	r.Use(
		hlog.NewHandler(s.log),
		hlog.RequestHandler("request"),
		middleware.DefaultCompress,
		middleware.SetHeader(
			"X-Contrax-Backend-Version",
			version.CreateSlug(),
		),
		logRequest,
	)
	handler := contraxapi.HandlerFromMux(s, r)

	s.log.Info().Str("address", s.options.Address).Msg("Starting HTTP server")
	return http.ListenAndServe(s.options.Address, handler)
}

func logRequest(h http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			hlog.FromRequest(r).Debug().Msg("Got a request")
			h.ServeHTTP(w, r)
		},
	)
}

type apiError struct {
	code       int32
	message    string
	identifier string
}

// jsonError writes a JSON error to the client, with an HTTP status code and error message.
// If calling this from an HTTP handler, immediately return after calling this function.
func (s *Server) jsonError(ctx context.Context, w http.ResponseWriter, err apiError) {
	contraxErr := contraxapi.Error{
		Code:       err.code,
		Identifier: pointer.ToStringOrNil(err.identifier),
		Message:    "Error: " + err.message,
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(int(err.code))
	if err := json.NewEncoder(w).Encode(contraxErr); err != nil {
		log.Ctx(ctx).Err(err).Msg("Could not encode HTTP error response body as JSON")
	}
}

// jsonResponse responds with HTTP status code `code` and JSON response body `v`.
// If serialization fails, a JSON error is written to the client.
func (s *Server) jsonResponse(ctx context.Context, w http.ResponseWriter, code int, v interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	if err := json.NewEncoder(w).Encode(v); err != nil {
		// Make sure we log the method which is trying to serialize, not this method.
		// By default, Zerolog skips two frames to get to the caller. In this case,
		// we want to get the function which has called this function, jsonResponse.
		const skipFrameCount = 3

		logger := log.Ctx(ctx).With().CallerWithSkipFrameCount(skipFrameCount).Logger()
		logger.Err(err).Msg("(api.Server#jsonResponse) Unable to serialize response")

		s.jsonError(ctx, w, apiError{
			code:    http.StatusInternalServerError,
			message: "unable to serialize response",
		})
	}
}

// parseRequestJSON parses the request body as JSON into v, and writes a JSON error to the client
// if this fails. If calling this function from a HTTP handler, immediately return if false is returned.
func (s *Server) parseRequestJSON(w http.ResponseWriter, r *http.Request, v interface{}) bool {
	if r.Header.Get("Content-Type") != "application/json" {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusBadRequest,
			message: "expected Content-Type to be application/json",
		})
		return false
	}
	if err := json.NewDecoder(r.Body).Decode(v); err != nil {
		hlog.FromRequest(r).Err(err).Msg("Unable to decode request body JSON")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusBadRequest,
			message: "unable to decode request body JSON",
		})
		return false
	}
	return true
}

// CreateAccount implements the createAccount API call.
func (s *Server) CreateAccount(w http.ResponseWriter, r *http.Request) {
	var account contraxapi.CreateAccountJSONRequestBody
	if !s.parseRequestJSON(w, r, &account) {
		return
	}

	err := chekra.CreateAccountRequest(&account)
	if err != nil {
		s.jsonError(r.Context(), w, apiError{code: http.StatusBadRequest, message: err.Error()})
		return
	}

	authInfo := s.db.NewAuth(account.Password)

	// Create the new user.
	user, err := s.db.CreateUserAndAuth(r.Context(), &database.User{
		Email:     account.Email,
		FirstName: account.FirstName,
		LastName:  account.LastName,
	}, authInfo)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not create user")
		s.jsonError(r.Context(), w, apiError{code: http.StatusInternalServerError, message: "could not create user"})
		return
	}

	// Return the user, with the ID added.
	rspUser := contraxapi.User{
		Id:        user.ID,
		Email:     user.Email,
		FirstName: user.FirstName,
		LastName:  user.LastName,
	}
	s.jsonResponse(r.Context(), w, http.StatusCreated, rspUser)
}

// GetLoan is the implementation of the getLoan API call.
func (s *Server) GetLoan(w http.ResponseWriter, r *http.Request) {
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	loanID := r.Context().Value("id").(int64)
	loan, err := s.db.GetLoan(r.Context(), loanID)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not retrieve loan from database")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusBadRequest,
			message: "could not retrieve loan",
		})
		return
	}
	if loan.FromUserID != authInfo.user.ID && loan.ToUserID != authInfo.user.ID {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusUnauthorized,
			message: "unauthorized to read loan",
		})
		return
	}

	apiLoan := convert.LoadedLoanToAPI(loan)
	s.jsonResponse(r.Context(), w, http.StatusOK, apiLoan)
}

// GetLoans is the implementation of the getLoans API call.
func (s *Server) GetLoans(w http.ResponseWriter, r *http.Request) {
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	params := contraxapi.ParamsForGetLoans(r.Context())
	convertedParams, err := database.GetLoansParamsFromAPI(params)
	if err != nil {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusBadRequest,
			message: "invalid request parameters: " + err.Error(),
		})
		return
	}

	loans, err := s.db.GetLoans(r.Context(), authInfo.user.ID, convertedParams)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not get loans from the database")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusNotFound,
			message: "could not get loans",
		})
		return
	}

	apiLoans := convert.LoadedLoansToAPI(loans...)
	addLoansCursor(&apiLoans, convertedParams)

	s.jsonResponse(r.Context(), w, http.StatusOK, apiLoans)
}

// GetBalance is the implementation of the getBalance API call.
func (s *Server) GetBalance(w http.ResponseWriter, r *http.Request) {
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	balance, err := s.db.GetBalance(r.Context(), authInfo.user.ID)
	if err != nil {
		log.Err(err).Msg("Could not calculate balance")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusInternalServerError,
			message: "could not calculate balance",
		})
		return
	}

	apiBalance := convert.BalanceToAPI(balance)
	s.jsonResponse(r.Context(), w, http.StatusOK, apiBalance)
}

// CreateLoan is the implementation of the createLoan API call.
func (s *Server) CreateLoan(w http.ResponseWriter, r *http.Request) {
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	var loan contraxapi.CreateLoanJSONRequestBody
	if !s.parseRequestJSON(w, r, &loan) {
		return
	}

	if err := chekra.CreateLoanRequest(&loan); err != nil {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusBadRequest,
			message: err.Error(),
		})
		return
	}

	otherUser, err := s.db.GetUserByEmail(r.Context(), loan.OtherUserEmail)
	if err != nil {
		s.jsonError(r.Context(), w, apiError{
			code:       http.StatusBadRequest,
			message:    "could not find other user",
			identifier: "USER_NOT_FOUND",
		})
		return
	}

	var fromUserID, toUserID int64
	var approvalUser database.ApprovalUser
	if loan.ReceivingUser == "current" {
		fromUserID = authInfo.user.ID
		toUserID = otherUser.ID
		approvalUser = database.ApprovalUserTo
	} else if loan.ReceivingUser == "other" {
		fromUserID = otherUser.ID
		toUserID = authInfo.user.ID
		approvalUser = database.ApprovalUserFrom
	}

	// Create the new loan.
	dbLoan, err := s.db.CreateLoan(r.Context(), &database.Loan{
		Amount:       loan.Amount,
		FromUserID:   fromUserID,
		ToUserID:     toUserID,
		Description:  stringPointerToString(loan.Description),
		ApprovalUser: approvalUser,
	})
	if err != nil {
		hlog.FromRequest(r).Error().Err(err).Msg("Could not create loan")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusInternalServerError,
			message: "could not create loan",
		})
		return
	}

	// Return the loan, with the ID added.
	rspLoan := convert.LoadedLoanToAPI(dbLoan)
	s.jsonResponse(r.Context(), w, http.StatusCreated, rspLoan)
}

// UpdateLoanMutation is the implementation of the updateLoanMutation API call.
func (s *Server) UpdateLoanMutation(w http.ResponseWriter, r *http.Request) {
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	mutationID := r.Context().Value("id").(int64)

	mut, err := s.db.GetMutation(r.Context(), mutationID)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not retrieve loan mutation status from database")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusNotFound,
			message: "could not retrieve loan mutation status loan",
		})
		return
	}

	authorized, err := s.db.IsUserAuthorizedToReadLoan(r.Context(), authInfo.user.ID, mut.LoanID)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not retrieve loan authorization status from database")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusInternalServerError,
			message: "could not retrieve loan authorization status loan",
		})
		return
	}
	if !authorized {
		hlog.FromRequest(r).Err(err).Msg("User is unauthorized to read loan")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusUnauthorized,
			message: "unauthorized to read loan",
		})
		return
	}

	params := contraxapi.ParamsForUpdateLoanMutation(r.Context())
	if params.Accept {
		if mut.ByUserID == authInfo.user.ID {
			hlog.FromRequest(r).Err(err).Msg("User is unauthorized to update loan mutation")
			s.jsonError(r.Context(), w, apiError{
				code:    http.StatusUnauthorized,
				message: "unauthorized to update loan mutation",
			})
			return
		}
		if !s.acceptLoanMutation(r.Context(), w, mutationID) {
			return
		}
	} else {
		// The user should be able to reject the mutation, only not to accept it.
		if !s.rejectLoanMutation(r.Context(), w, mutationID) {
			return
		}
	}

	patchedLoan, err := s.db.GetLoan(r.Context(), mut.LoanID)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not retrieve patched loan")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusNotFound,
			message: "could not retrieve patched loan",
		})
		return
	}

	apiPatchedLoan := convert.LoadedLoanToAPI(patchedLoan)
	s.jsonResponse(r.Context(), w, http.StatusOK, apiPatchedLoan)
}

// CreateLoanMutation is the implementation of the createLoanMutation API call.
func (s *Server) CreateLoanMutation(w http.ResponseWriter, r *http.Request) {
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	var mut contraxapi.CreateLoanMutationRequest
	if !s.parseRequestJSON(w, r, &mut) {
		return
	}

	if err := chekra.CreateLoanMutationRequest(&mut); err != nil {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusBadRequest,
			message: err.Error(),
		})
		return
	}

	loanID := r.Context().Value("id").(int64)
	loan, err := s.db.GetLoan(r.Context(), loanID)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not retrieve loan from database")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusNotFound,
			message: "could not retrieve loan",
		})
		return
	}
	if loan.FromUserID != authInfo.user.ID && loan.ToUserID != authInfo.user.ID {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusUnauthorized,
			message: "unauthorized to read loan",
		})
		return
	}

	deleted := false
	if mut.Approved != nil && isApprovalUser(authInfo.user.ID, &loan.Loan) && !loan.ApprovedAt.Valid {
		if *mut.Approved {
			err = s.db.ApproveLoan(r.Context(), loanID)
		} else {
			err = s.db.RejectLoan(r.Context(), loanID)
			deleted = true
		}
		if err != nil {
			s.jsonError(r.Context(), w, apiError{
				code:    http.StatusInternalServerError,
				message: "could not approve or reject loan",
			})
			return
		}
	}

	if (mut.Amount == nil && mut.Fulfilled == nil && mut.Description == nil) || deleted {
		s.jsonResponse(r.Context(), w, http.StatusNotModified, loan)
		return
	}

	dbMut := convert.CreateLoanMutationRequestFromAPI(&mut)
	dbMut.CreatedAt = time.Now()
	dbMut.LoanID = loanID
	dbMut.ByUserID = authInfo.user.ID

	loadedMut, err := s.db.CreateMutation(r.Context(), &dbMut)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not create mutation")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusInternalServerError,
			message: "could not create mutation",
		})
		return
	}

	apiMutation := convert.LoadedMutationToAPI(loadedMut)
	s.jsonResponse(r.Context(), w, http.StatusOK, apiMutation)
}

func isApprovalUser(userID int64, loan *database.Loan) bool {
	return (loan.ApprovalUser == database.ApprovalUserFrom && loan.FromUserID == userID) ||
		(loan.ApprovalUser == database.ApprovalUserTo && loan.ToUserID == userID)
}

// GetLoanMutations is the implementation of the getLoanMutations API call.
func (s *Server) GetLoanMutations(w http.ResponseWriter, r *http.Request) {
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	params := contraxapi.ParamsForGetLoanMutations(r.Context())
	loanID := r.Context().Value("id").(int64)

	convertedParams, err := database.GetMutationsParamsFromAPI(params)
	if err != nil {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusBadRequest,
			message: fmt.Sprintf("could not parse parameters: %v", err.Error()),
		})
		return
	}

	authorized, err := s.db.IsUserAuthorizedToReadLoan(r.Context(), authInfo.user.ID, loanID)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not retrieve loan authorization status from database")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusInternalServerError,
			message: "could not retrieve loan authorization status loan",
		})
		return
	}

	if !authorized {
		hlog.FromRequest(r).Err(err).Msg("User is unauthorized to read loan")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusUnauthorized,
			message: "unauthorized to read loan",
		})
		return
	}

	mutations, err := s.db.GetMutations(r.Context(), loanID, convertedParams)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not retrieve mutations from database")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusBadRequest,
			message: "could not retrieve loan mutations",
		})
		return
	}

	apiMutations := convert.LoadedMutationsToAPI(mutations)
	addMutationsCursor(&apiMutations)

	s.jsonResponse(r.Context(), w, http.StatusOK, apiMutations)
}

// GetLoanMutation is the implementation of the getLoanMutation API call.
func (s *Server) GetLoanMutation(w http.ResponseWriter, r *http.Request) {
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	mutationID := r.Context().Value("id").(int64)
	mutation, err := s.db.GetMutation(r.Context(), mutationID)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not retrieve mutation from database")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusBadRequest,
			message: "could not retrieve loan mutation",
		})
		return
	}
	if mutation.Loan.FromUserID != authInfo.user.ID && mutation.Loan.ToUserID != authInfo.user.ID {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusUnauthorized,
			message: "unauthorized to read loan mutation",
		})
		return
	}

	apiMutation := convert.LoadedMutationToAPI(mutation)
	s.jsonResponse(r.Context(), w, http.StatusOK, apiMutation)
}

// GetUser is the implementation of the getUser API call.
func (s *Server) GetUser(w http.ResponseWriter, r *http.Request) {
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	user := authInfo.user
	userID := r.Context().Value("id").(int64)
	if userID != 0 {
		var err error
		user, err = s.repo.GetUserByID(r.Context(), userID)
		if err != nil {
			s.jsonError(r.Context(), w, apiError{
				code:    http.StatusNotFound,
				message: "could not get user",
			})
			return
		}
	}

	apiUser := convert.UserToAPI(user)
	s.jsonResponse(r.Context(), w, http.StatusOK, &apiUser)
}

// RefreshToken is the implementation of the refreshToken API call.
func (s *Server) RefreshToken(w http.ResponseWriter, r *http.Request) {
	// To refresh the token, the user must already provide a valid token.
	// If auth returns true, we know we can proceed to delete the old token and create a new one.
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	// We can safely dereference authInfo.token because the refreshToken call either uses
	// apiKeyAuth or cookieAuth, which both make sure authInfo.token gets a non-null value.
	err := s.ts.DeleteToken(*authInfo.token)
	if err != nil {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusInternalServerError,
			message: "could not invalidate old token",
		})
		return
	}

	// Create the new token after the old one has been deleted.
	token, expiresIn, err := s.ts.CreateToken(authInfo.user.ID)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not create token")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusInternalServerError,
			message: "could not create token",
		})
		return
	}

	// Set the session cookie, so the browser will automatically remember this new token.
	s.setSessionCookie(w, token, time.Now().Add(expiresIn))
	rspToken := contraxapi.Token{
		Token:     token.String(),
		ExpiresIn: int64(expiresIn.Seconds()),
	}
	s.jsonResponse(r.Context(), w, http.StatusCreated, rspToken)
}

// LogIn is the implementation of the logIn API call.
func (s *Server) LogIn(w http.ResponseWriter, r *http.Request) {
	// Make sure the user has logged in (uses basic auth).
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	// The user has successfully logged in, a new token can be created.
	token, expiresIn, err := s.ts.CreateToken(authInfo.user.ID)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not create token")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusInternalServerError,
			message: "could not create token",
		})
		return
	}

	// Set the session cookie, so the browser will automatically remember the token.
	s.setSessionCookie(w, token, time.Now().Add(expiresIn))
	rspToken := contraxapi.Token{
		Token:     token.String(),
		ExpiresIn: int64(expiresIn.Seconds()),
	}
	s.jsonResponse(r.Context(), w, http.StatusCreated, rspToken)
}

// LogOut is the implementation of the logOut API call.
func (s *Server) LogOut(w http.ResponseWriter, r *http.Request) {
	// Make sure the user has logged in (uses basic auth).
	authInfo, ok := s.auth(w, r)
	if !ok {
		return
	}

	err := s.ts.DeleteToken(*authInfo.token)
	if err != nil {
		hlog.FromRequest(r).Err(err).Msg("Could not delete token")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusInternalServerError,
			message: "could not delete token",
		})
		return
	}

	// Invalidate the session cookie by making it expire in the past.
	s.setSessionCookie(w, *authInfo.token, time.Unix(0, 0))

	// No JSON response, just an OK status.
	w.WriteHeader(http.StatusOK)
}

func (s *Server) rejectLoanMutation(ctx context.Context, w http.ResponseWriter, mutationID int64) bool {
	err := s.db.RejectLoanMutation(ctx, mutationID)
	if err != nil {
		s.jsonError(ctx, w, apiError{
			code:    http.StatusInternalServerError,
			message: "could not reject loan mutation",
		})
		return false
	}
	return true
}

func (s *Server) acceptLoanMutation(ctx context.Context, w http.ResponseWriter, mutationID int64) bool {
	err := s.db.AcceptLoanMutation(ctx, mutationID)
	if err != nil {
		s.jsonError(ctx, w, apiError{
			code:    http.StatusInternalServerError,
			message: "could not accept loan mutation",
		})
		return false
	}
	return true
}

func stringPointerToString(s *string) string {
	if s == nil {
		return ""
	}
	return *s
}
