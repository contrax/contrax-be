package api

import (
	"flag"

	"gitlab.com/contrax/contrax-be/internal/repository"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"

	"gitlab.com/contrax/contrax-be/internal/database"
	"gitlab.com/contrax/contrax-be/internal/tokenstore"
)

// Factory is the API server factory.
type Factory struct {
	options *Options
	db      *database.Wrapper
	ts      *tokenstore.TokenStore
	log     zerolog.Logger
	repo    *repository.Repository
}

// NewFactory creates a new API server factory.
func NewFactory() *Factory {
	return &Factory{
		options: &Options{},
		log:     zerolog.Nop(),
		db:      nil,
	}
}

// AddFlags adds all command-line flags from Options to the command-line flags.
func (f *Factory) AddFlags(flagSet *flag.FlagSet) {
	AddFlags(flagSet)
}

// InitFromViper initializes the options from the Viper configuration.
func (f *Factory) InitFromViper(v *viper.Viper) error {
	f.options.InitFromViper(v)
	return nil
}

// Initialize initialized the API server factory.
func (f *Factory) Initialize(
	log zerolog.Logger,
	db *database.Wrapper,
	ts *tokenstore.TokenStore,
	repo *repository.Repository,
) {
	f.log = log
	f.db = db
	f.ts = ts
	f.repo = repo
}

// CreateServer creates the API server. Should only be called after initialization.
func (f *Factory) CreateServer() *Server {
	return &Server{*f.options, f.log, f.repo, f.db, f.ts}
}
