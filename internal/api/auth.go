package api

import (
	"encoding/base64"
	"errors"
	"net/http"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/hlog"

	"gitlab.com/contrax/contrax-be/internal/database"
)

const (
	// The user has not used the authentication method (properly).
	errNotPresent errorString = "not present"
	// The user has provided invalid information for the authentication method.
	errInvalid errorString = "invalid"

	// The cookie in which the base64-encoded session / API token is stored.
	contraxSessionCookie string = "contraxsess"
)

// errorString is a type that implements the error interface and can be used for constant errors.
type errorString string

// Error converts the errorString to an error.
func (e errorString) Error() string {
	return string(e)
}

// authInfo contains information about the authenticated user.
type authInfo struct {
	// The authenticated user. Always set.
	user *database.User
	// The token which the user has authenticated with.
	// Set if call is using cookieAuth or apiKeyAuth.
	token *uuid.UUID
}

// authFunc is a function which can authenticate the user and return information on them.
type authFunc func(w http.ResponseWriter, r *http.Request) (*authInfo, error)

// auth attempts to authenticate the user, defined as per the OpenAPI specification.
// Because we know that oapi-codegen sets scopes in the context, it is possible
// to retrieve those and check the authentication methods enabled for the call,
// which can then all be used in order to check if the user has made any attempt
// to authenticate themselves. If no authentication methods are provided, the call
// is automatically authorized but no user or token is provided in the returned authInfo.
func (s *Server) auth(w http.ResponseWriter, r *http.Request) (*authInfo, bool) {
	// Because oapi-codegen automatically adds the scopes for the call to the request context,
	// we can check if they are present by doing a type assertion on the (maybe present) value.
	_, apiKeyAuth := r.Context().Value("apiKeyAuth.Scopes").([]string)
	_, cookieAuth := r.Context().Value("cookieAuth.Scopes").([]string)
	_, basicAuth := r.Context().Value("basicAuth.Scopes").([]string)
	if !(apiKeyAuth || cookieAuth || basicAuth) {
		// The call requires no authentication, we might as well return now.
		return new(authInfo), true
	}

	// Attempt to authenticate with all enabled methods for the call.
	// A slice is created which describes all authentication methods and whether they
	// are enabled or not. Each enabled authentication method is attempted as long as
	// errNotPresent is returned, otherwise, the function returns.
	methods := []struct {
		authFunc authFunc
		enabled  bool
	}{
		{authFunc: s.apiKeyAuth, enabled: apiKeyAuth},
		{authFunc: s.cookieAuth, enabled: cookieAuth},
		{authFunc: s.basicAuth, enabled: basicAuth},
	}
	for _, method := range methods {
		if method.enabled {
			// Attempt to authenticate.
			info, err := method.authFunc(w, r)
			if err == nil {
				// Authentication successful.
				return info, true
			}
			if !errors.Is(err, errNotPresent) {
				// The user has tried to authenticate using this method, but authentication
				// has failed, and an error should have been written to the client.
				return nil, false
			}
		}
	}

	// None of the methods worked somehow.
	s.jsonError(r.Context(), w, apiError{
		code:    http.StatusUnauthorized,
		message: "unauthorized",
	})
	return nil, false
}

// apiKeyAuth implements API key authentication.
// It uses the value of the X-API-Key header, and if this is set, uses checkAPIKey for
// checking the validity of the provided key.
func (s *Server) apiKeyAuth(w http.ResponseWriter, r *http.Request) (*authInfo, error) {
	// Check if the X-API-Key HTTP header contains a value, and if so, attempt to authenticate
	// using this method. If successful, return with the authentication information.
	token := r.Header.Get("X-API-Key")
	if token == "" {
		return nil, errNotPresent
	}
	info, ok := s.checkAPIKey(w, r, token)
	if !ok {
		return nil, errInvalid
	}
	return info, nil
}

// cookieAuth implements cookie authentication.
// It fetches the contraxsess cookie from the HTTP headers (the Cookie header) and
// base64-decodes it, after which it uses checkAPIKey to check the validity of the API key in the cookie.
func (s *Server) cookieAuth(w http.ResponseWriter, r *http.Request) (*authInfo, error) {
	// Try to find the cookie.
	sessCookie, err := r.Cookie(contraxSessionCookie)
	if err != nil {
		return nil, errNotPresent
	}

	// The cookie value is base64-encoded, so it needs to be decoded before the token can be parsed.
	base64Str := strings.TrimSpace(sessCookie.Value)
	token, err := base64.URLEncoding.DecodeString(base64Str)
	if err != nil {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusBadRequest,
			message: "invalid encoding for API key in cookie " + contraxSessionCookie,
		})
		return nil, errInvalid
	}

	// Check the validity of the token, and the user associated with it.
	tokenStr := strings.TrimSpace(string(token))
	info, ok := s.checkAPIKey(w, r, tokenStr)
	if !ok {
		return nil, errInvalid
	}
	return info, nil
}

// cookieAuth implements basic authentication (username + password).
// First, it loads the basic auth information (username / email + password),
// tries to fetch the user and then load the authentication information for the user.
// It uses the authentication information to check the password which has been provided
// using the basic auth and if correct, authorizes the user.
func (s *Server) basicAuth(w http.ResponseWriter, r *http.Request) (*authInfo, error) {
	// Load the basic authentication information from the header.
	email, pass, ok := r.BasicAuth()
	if !ok {
		return nil, errNotPresent
	}

	// Get the user by their email, so we can use their ID to load the password hash and salt.
	user, err := s.db.GetUserByEmail(r.Context(), email)
	if err != nil {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusNotFound,
			message: "user not found",
		})
		return nil, errInvalid
	}

	// Check if the user ID and password check out.
	authorized, err := s.db.CheckAuth(r.Context(), user.ID, pass)
	if err != nil {
		hlog.FromRequest(r).Error().
			Err(err).Int64("userID", user.ID).
			Msg("Could not check user authentication with database")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusUnauthorized,
			message: "could not log in",
		})
		return nil, errInvalid
	}
	if !authorized {
		// The check was successful, but the user is not authorized. Either the email or the password is incorrect.
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusUnauthorized,
			message: "invalid username / password combination",
		})
		return nil, errInvalid
	}

	return &authInfo{user: user}, nil
}

// checkAPIKey checks an API key's validity, and retrieves the user whose it is.
// It is used by both apiKeyAuth and cookieAuth, because both use an API token.
// The API key / token has to be in valid UUID format, which is first parsed and then
// used to fetch the associated user. If a user is associated (the token is valid) the user
// is authorized, and the information on the associated user is loaded (into the returned authInfo).
func (s *Server) checkAPIKey(w http.ResponseWriter, r *http.Request, apiKey string) (*authInfo, bool) {
	// Check if the user has provided a valid token.
	uid, err := uuid.Parse(apiKey)
	if err != nil {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusBadRequest,
			message: "invalid key format",
		})
		return nil, false
	}

	// Find the token in the token store to get the associated user.
	userID, _, err := s.ts.GetTokenUser(uid)
	if err != nil {
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusUnauthorized,
			message: "invalid token",
		})
		return nil, false
	}

	// Load the information on the user associated with the token.
	user, err := s.repo.GetUserByID(r.Context(), userID)
	if err != nil {
		hlog.FromRequest(r).Error().
			Err(err).Int64("userID", userID).
			Msg("Could not fetch user by their ID (associated with token)")
		s.jsonError(r.Context(), w, apiError{
			code:    http.StatusNotFound,
			message: "token user not found",
		})
		return nil, false
	}

	return &authInfo{
		user:  user,
		token: &uid,
	}, true
}

// setSessionCookie sets the session cookie for the HTTP response, such that the browser will save it.
func (s *Server) setSessionCookie(w http.ResponseWriter, token uuid.UUID, expires time.Time) {
	tokenString := token.String()

	// The cookie value is base64-encoded.
	cookieValue := base64.URLEncoding.EncodeToString([]byte(tokenString))

	http.SetCookie(w, &http.Cookie{
		Name:    contraxSessionCookie,
		Value:   cookieValue,
		Expires: expires,
	})
}
