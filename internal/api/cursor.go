package api

import (
	"strconv"

	"gitlab.com/contrax/contrax-be/internal/database"

	"github.com/AlekSi/pointer"
	contraxapi "gitlab.com/contrax/contrax-be/pkg/apigen"
)

func addLoansCursor(loans *contraxapi.Loans, params *database.GetLoansParams) {
	loansSlice := *loans.Data
	if len(loansSlice) > 0 {
		lastLoan := loansSlice[len(loansSlice)-1]
		switch params.OrderBy {
		case "amount":
			loans.NextCursor = pointer.ToString(strconv.FormatFloat(lastLoan.Amount, 'G', -1, 64))
		case "created_at":
			loans.NextCursor = pointer.ToString(lastLoan.CreatedAt.String())
		}
	}
}

func addMutationsCursor(mutations *contraxapi.LoanMutations) {
	mutationSlice := *mutations.Data
	if len(mutationSlice) > 0 {
		lastMutation := mutationSlice[len(mutationSlice)-1]
		mutations.NextCursor = pointer.ToString(lastMutation.CreatedAt.String())
	}
}
