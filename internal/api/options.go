package api

import (
	"flag"

	"github.com/spf13/viper"
)

const apiAddress = "api.address"

// Options contains the configuration for the API server.
type Options struct {
	// The address which the API server should bind to.
	Address string
}

// AddFlags adds command-line flags for the API server.
func AddFlags(flagSet *flag.FlagSet) {
	flagSet.String(apiAddress, ":8080", "Address which the API server binds to")
}

// InitFromViper loads the configuration options from Viper.
func (opts *Options) InitFromViper(v *viper.Viper) *Options {
	opts.Address = v.GetString(apiAddress)
	return opts
}
