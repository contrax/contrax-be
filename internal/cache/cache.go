package cache

import (
	"context"
	"errors"
	"strconv"
	"strings"

	"github.com/go-redis/cache/v7"
	"github.com/go-redis/redis/v7"
	"github.com/rs/zerolog"
	"github.com/vmihailenco/msgpack/v4"
	"gitlab.com/contrax/contrax-be/internal/database"
)

// Cache is an abstraction for caching Contrax database items in Redis.
type Cache struct {
	codec *cache.Codec
	log   zerolog.Logger
	opts  *Options

	cache chan interface{}
}

// Connect to Redis to create the token store.
func Connect(opts *Options, log zerolog.Logger) (*Cache, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     opts.Address,
		Password: opts.Password,
		DB:       opts.Database,
	})

	_, err := client.Ping().Result()
	if err != nil {
		return nil, errors.New("(cache.Connect) could not ping Redis")
	}

	return &Cache{
		codec: &cache.Codec{
			Redis:     client,
			Marshal:   msgpack.Marshal,
			Unmarshal: msgpack.Unmarshal,
		},
		log:   log,
		opts:  opts,
		cache: make(chan interface{}),
	}, nil
}

// Run runs the cache.
func (c *Cache) Run() {
	for it := range c.cache {
		if user, ok := it.(*database.User); ok {
			err := c.putUser(user)
			if err != nil {
				c.log.Err(err).Msg("Could not cache user")
			}
		}
	}
}

// GetUserByID retrieves a user from the cache, by its ID.
// An error is returned if the user does not exist.
func (c *Cache) GetUserByID(ctx context.Context, id int64) (*database.User, error) {
	k := key("user", "id", strconv.FormatInt(id, 10))

	var user database.User
	err := c.codec.GetContext(ctx, k, &user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

// PutUser caches a user.
func (c *Cache) PutUser(u *database.User) {
	c.cache <- u
}

// putUser caches a user.
func (c *Cache) putUser(u *database.User) error {
	c.log.Debug().Int64("id", u.ID).Msg("Caching user")
	k := key("user", "id", strconv.FormatInt(u.ID, 10))
	return c.codec.Set(&cache.Item{
		Key:        k,
		Object:     u,
		Expiration: c.opts.Expiration,
	})
}

// InvalidateCachedUser invalidates a user.
func (c *Cache) InvalidateUser(ctx context.Context, u *database.User) error {
	return c.invalidateUserByID(ctx, u.ID)
}

// invalidateUserByID invalidates a user by its ID.
func (c *Cache) invalidateUserByID(ctx context.Context, id int64) error {
	k := key("user", "id", strconv.FormatInt(id, 10))
	return c.codec.DeleteContext(ctx, k)
}

// key creates a new cache key, where the identity would be e.g. 'user',
// index would be e.g. 'id', and unique would be the value of the id,
// which would make for the key 'user:id:1'.
func key(entity, index, unique string) string {
	var b strings.Builder

	// Two colons, one between entity and index, one between index and unique.
	const numColons = 2

	b.Grow(len(entity) + len(index) + len(unique) + numColons)

	b.WriteString("cache:")
	b.WriteString(entity)
	b.WriteByte(':')
	b.WriteString(index)
	b.WriteByte(':')
	b.WriteString(unique)

	return b.String()
}
