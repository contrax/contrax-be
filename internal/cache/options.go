package cache

import (
	"flag"
	"fmt"
	"time"

	"github.com/spf13/viper"
)

const (
	cacheAddress    = "cache.address"
	cachePassword   = "cache.password"
	cacheDatabase   = "cache.database"
	cacheExpiration = "cache.token-expiration"
)

// Options contains the configuration options for the cache (Redis) connection.
type Options struct {
	Address  string
	Password string
	Database int

	Expiration time.Duration
}

const defaultCacheExpiration = time.Minute * 30

// AddFlags adds the command-line flags for the database connection configuration.
func AddFlags(flagSet *flag.FlagSet) {
	flagSet.String(cacheAddress, "", "Address of the Redis server")
	flagSet.String(cachePassword, "", "Password for the Redis serer")
	flagSet.Int(cacheDatabase, 0, "Database number for saving cache items in in the Redis server")
	flagSet.Duration(cacheExpiration, defaultCacheExpiration, "Amount of time before a cache entry is set to expire")
}

// InitFromViper loads the database connection options from Viper.
func (opts *Options) InitFromViper(v *viper.Viper) (*Options, error) {
	address := v.GetString(cacheAddress)
	password := v.GetString(cachePassword)
	database := v.GetInt(cacheDatabase)
	tokenExpiration := v.GetDuration(cacheExpiration)

	if address == "" {
		return opts, fmt.Errorf("%s is not set", cacheAddress)
	}

	opts.Address = address
	opts.Password = password
	opts.Database = database
	opts.Expiration = tokenExpiration

	return opts, nil
}
