package cache

import (
	"flag"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
)

// Factory is a factory for the cache.
type Factory struct {
	options *Options
	log     zerolog.Logger
}

// NewFactory creates a new cache factory.
func NewFactory() *Factory {
	return &Factory{
		options: &Options{},
		log:     zerolog.Nop(),
	}
}

// AddFlags adds command-line flags for the cache configuration options.
func (f *Factory) AddFlags(flagSet *flag.FlagSet) {
	AddFlags(flagSet)
}

// InitFromViper loads the cache configuration from Viper.
func (f *Factory) InitFromViper(v *viper.Viper) error {
	_, err := f.options.InitFromViper(v)
	return err
}

// Initialize initializes the factory.
func (f *Factory) Initialize(log zerolog.Logger) {
	f.log = log
}

// CreateCache creates the cache.
func (f *Factory) CreateCache() (*Cache, error) {
	return Connect(f.options, f.log)
}
