// Package factories provides utilities for working with factories (defined in pkg/factory).
package factories

import (
	"flag"

	"github.com/hashicorp/go-multierror"
	"github.com/spf13/viper"

	"gitlab.com/contrax/contrax-be/pkg/factory"
)

// Collection is a collection factories used to initialize multiple factories at once.
type Collection struct {
	factories []factory.Interface
}

// NewCollection creates a new, initialized Collection instance.
func NewCollection(f ...factory.Interface) *Collection {
	return &Collection{
		factories: f,
	}
}

// AddFlags calls all factories to add command-line flags.
func (p *Collection) AddFlags(flagSet *flag.FlagSet) {
	for _, f := range p.factories {
		f.AddFlags(flagSet)
	}
}

// InitFromViper initializes all factories with configuration from Viper.
func (p *Collection) InitFromViper(v *viper.Viper) error {
	var err error
	for _, f := range p.factories {
		initErr := f.InitFromViper(v)
		if initErr != nil {
			// If an error occurs, don't just quit but present the user with all their wrongdoings.
			err = multierror.Append(err, initErr)
		}
	}
	return err
}
