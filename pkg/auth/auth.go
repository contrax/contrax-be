package auth

import (
	"crypto/rand"

	"github.com/rs/zerolog"

	"golang.org/x/crypto/sha3"
)

// HashedPassword contains a hashed password with an associated salt.
type HashedPassword struct {
	Hash [64]byte
	Salt [64]byte
}

// PasswordHasher hashes passwords.
type PasswordHasher struct {
	log zerolog.Logger
}

// NewPasswordHasher creates a new password hasher.
func NewPasswordHasher(log zerolog.Logger) PasswordHasher {
	return PasswordHasher{log}
}

// NewHashedPassword generates a new salt for a password and immediately hashes it after.
func (ph PasswordHasher) NewHashedPassword(password string) *HashedPassword {
	var hp HashedPassword

	_, err := rand.Read(hp.Salt[:])
	if err != nil {
		ph.log.Panic().Err(err).Msg("Could not read from crypto/rand")
	}
	ph.Hash(password, hp.Salt[:], hp.Hash[:])

	return &hp
}

// Hash hashes a password with a predefined salt, and writes the hash into `hash`, till `hash` is filled up.
func (ph PasswordHasher) Hash(password string, salt []byte, hash []byte) {
	h := sha3.NewShake256()

	_, err := h.Write([]byte(password))
	if err != nil {
		ph.log.Panic().Err(err).Msg("Could not hash password")
	}

	_, err = h.Write(salt)
	if err != nil {
		ph.log.Panic().Err(err).Msg("Could not hash password")
	}

	_, err = h.Read(hash)
	if err != nil {
		ph.log.Panic().Err(err).Msg("Could not read hashed password")
	}
}
