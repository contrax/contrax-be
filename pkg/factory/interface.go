package factory

import (
	"flag"

	"github.com/spf13/viper"
)

// Interface defines a generic interface for a factory.
type Interface interface {
	AddFlags(flagSet *flag.FlagSet)
	InitFromViper(v *viper.Viper) error
}
