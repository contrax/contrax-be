BEGIN;

CREATE TABLE user
(
    id         INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email      VARCHAR(255) UNIQUE KEY NOT NULL,
    first_name VARCHAR(255)            NOT NULL,
    last_name  VARCHAR(255)            NOT NULL
) ENGINE = innodb;

CREATE TABLE auth
(
    user_id   INT UNSIGNED PRIMARY KEY NOT NULL,
    pass_hash BINARY(64)               NOT NULL,
    pass_salt BINARY(64)               NOT NULL,

    FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE
) ENGINE = innodb;

CREATE TABLE loan
(
    id            INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    amount        DECIMAL(8, 2)       NOT NULL,
    from_user_id  INT UNSIGNED        NOT NULL,
    to_user_id    INT UNSIGNED        NOT NULL,
    approval_user ENUM ('from', 'to') NOT NULL,
    created_at    DATETIME            NOT NULL,
    approved_at   DATETIME,
    fulfilled_at  DATETIME,

    FOREIGN KEY (from_user_id) REFERENCES user (id) ON DELETE CASCADE,
    FOREIGN KEY (to_user_id) REFERENCES user (id) ON DELETE CASCADE
) ENGINE = innodb;

CREATE TABLE pending_loan_mutation
(
    id         INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    loan_id    INT UNSIGNED NOT NULL,
    by_user_id INT UNSIGNED NOT NULL,
    created_at DATETIME     NOT NULL,

    -- The contents of the mutation.
    amount     DECIMAL(8, 2),

    FOREIGN KEY (loan_id) REFERENCES loan (id) ON DELETE CASCADE,
    FOREIGN KEY (by_user_id) REFERENCES user (id) ON DELETE CASCADE
) ENGINE = innodb;

COMMIT;
