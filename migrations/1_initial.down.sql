BEGIN;

DROP TABLE pending_loan_mutation;
DROP TABLE loan;
DROP TABLE user;
DROP TABLE auth;

COMMIT;
