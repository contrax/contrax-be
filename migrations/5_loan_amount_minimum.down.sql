BEGIN;

ALTER TABLE loan
    DROP CONSTRAINT loan_amount_minimum;

ALTER TABLE pending_loan_mutation
    DROP CONSTRAINT loan_amount_minimum;

COMMIT;