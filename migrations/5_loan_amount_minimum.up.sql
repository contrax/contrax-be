BEGIN;

ALTER TABLE loan
    ADD CONSTRAINT loan_amount_minimum
        CHECK (amount >= 0.01);

ALTER TABLE pending_loan_mutation
    ADD CONSTRAINT loan_amount_minimum
        CHECK (amount >= 0.01);

COMMIT;