BEGIN;

ALTER TABLE loan
    MODIFY amount DECIMAL(8, 2) NOT NULL;
ALTER TABLE pending_loan_mutation
    MODIFY amount DECIMAL(8, 2);

COMMIT;