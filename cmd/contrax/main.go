package main

import (
	"flag"
	"strings"

	"github.com/go-chi/chi/middleware"
	_ "github.com/joho/godotenv/autoload"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"

	"gitlab.com/contrax/contrax-be/internal/api"
	"gitlab.com/contrax/contrax-be/internal/cache"
	"gitlab.com/contrax/contrax-be/internal/database"
	"gitlab.com/contrax/contrax-be/internal/factories"
	"gitlab.com/contrax/contrax-be/internal/repository"
	"gitlab.com/contrax/contrax-be/internal/tokenstore"

	"gitlab.com/contrax/contrax-be/internal/version"
)

// The entry point of the 'contrax' command. This is where the back-end starts.
func main() { //nolint:funlen
	// Configure the logger (globally).
	setupLogger()

	// Print the program version.
	version.Log()

	// We create factories, which provide configuration options and load them from
	// the environment or options provided on the command line.
	df := database.NewFactory()
	cf := cache.NewFactory()
	rf := repository.NewFactory()
	tsf := tokenstore.NewFactory()
	af := api.NewFactory()

	// Create all factories and add all command line flags before loading configuration.
	collection := factories.NewCollection(df, cf, rf, tsf, af)
	collection.AddFlags(flag.CommandLine)

	// Setup Viper such that the configuration can be loaded.
	setupViper()

	// Initialize all factories from the configuration loaded into Viper.
	err := collection.InitFromViper(viper.GetViper())
	if err != nil {
		log.Fatal().Err(err).Msg("Could not initialize")
	}

	// Initialize the database factory.
	df.Initialize(log.Logger)

	// Create the database wrapper.
	db, err := df.CreateWrapper()
	if err != nil {
		log.Fatal().Err(err).Msg("Could not initialize database")
	}
	log.Info().Msg("Connected to database")

	// Initialize the token store factory.
	tsf.Initialize(log.Logger)

	// Create the token store.
	ts, err := tsf.CreateTokenStore()
	if err != nil {
		log.Fatal().Err(err).Msg("Could not initialize token store")
	}
	log.Info().Msg("Connected to token store")

	// Initialize the cache factory.
	cf.Initialize(log.Logger)

	// Create the cache.
	cach, err := cf.CreateCache()
	if err != nil {
		log.Fatal().Err(err).Msg("Could not initialize cache")
	}
	go cach.Run()
	log.Info().Msg("Cache running")

	// Initialize the repository factory.
	rf.Initialize(log.Logger, cach, db)

	// Create the repository.
	repo := rf.CreateRepository()

	// Initialize the API factory.
	af.Initialize(log.Logger, db, ts, repo)

	// Create the API and serve it.
	srv := af.CreateServer()
	err = srv.ListenAndServe()
	if err != nil {
		log.Fatal().Err(err).Msg("Could not serve API")
	}
}

// Set the logger up globally so all logs are in one place and in one format.
func setupLogger() {
	// Make the log output look nice, instead of JSON.
	log.Logger = log.Output(zerolog.NewConsoleWriter()).With().
		Caller().
		Str("version", version.CreateSlug()).
		Logger()

	// Use Zerolog for Chi (the HTTP router).
	middleware.DefaultLogger = middleware.RequestLogger(&middleware.DefaultLogFormatter{
		Logger:  &log.Logger,
		NoColor: true,
	})
}

// Setup Viper.
func setupViper() {
	// Add the flag set initialized by the factories.
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	// Parse the command line flags.
	pflag.Parse()

	// Bind the command line flags to the configuration.
	err := viper.BindPFlags(pflag.CommandLine)
	if err != nil {
		log.Fatal().Err(err).Msg("Could not bind flags to Viper")
	}

	// Configuration options such as 'token-store.address' won't work as uppercase environment variables,
	// but should be in the form of TOKEN_STORE_ADDRESS.
	replacer := strings.NewReplacer(".", "_", "-", "_")
	viper.SetEnvKeyReplacer(replacer)
	// Load the environment variables as a configuration option source.
	viper.AutomaticEnv()
}
