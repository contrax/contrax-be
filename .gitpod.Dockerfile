FROM gitpod/workspace-full

USER gitpod

RUN sudo apt-get -q update \
 && DEBIAN_FRONTEND=noninteractive sudo apt-get install -yq mariadb-server redis-server
RUN sudo mkdir /usr/share/adminer \
 && sudo wget "http://www.adminer.org/latest.php" -O /usr/share/adminer/latest.php \
 && sudo ln -s /usr/share/adminer/latest.php /usr/share/adminer/adminer.php \ 
 && echo "Alias /adminer.php /usr/share/adminer/adminer.php" | sudo tee /etc/apache2/conf-available/adminer.conf \
 && sudo a2enconf adminer.conf