module gitlab.com/contrax/contrax-be

go 1.13

require (
	github.com/AlekSi/pointer v1.1.0
	github.com/FGRibreau/mailchecker/v3 v3.3.4
	github.com/Masterminds/squirrel v1.2.0
	github.com/davecgh/go-spew v1.1.1
	github.com/deepmap/oapi-codegen v1.3.6
	github.com/getkin/kin-openapi v0.2.0
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-redis/cache/v7 v7.0.2
	github.com/go-redis/redis/v7 v7.0.0-beta.4
	github.com/go-sql-driver/mysql v1.4.1
	github.com/golang-migrate/migrate/v4 v4.8.0
	github.com/google/uuid v1.1.1
	github.com/hashicorp/go-multierror v1.0.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.14 // indirect
	github.com/rs/zerolog v1.17.2
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.6.2
	github.com/vmihailenco/msgpack/v4 v4.2.0
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876
	google.golang.org/appengine v1.6.5 // indirect
)
