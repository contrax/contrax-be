# Contrax backend

contrax-be is het backend voor Contrax.

## Gebruikte bibliotheken

* [go-redis/redis](https://pkg.go.dev/github.com/go-redis/redis): Met Redis (een NoSQL-database) communiceren
* [go-sql-driver/mysql](https://pkg.go.dev/github.com/go-sql-driver/mysql): Met MySQL / MariaDB via [database/sql](https://pkg.go.dev/database/sql) communiceren
* [golang-migrate/migrate](https://pkg.go.dev/github.com/golang-migrate/migrate): Migraties (schemawijzigingen) automatisch doorvoeren bij opstart van de applicatie
* [google/uuid](https://pkg.go.dev/github.com/google/uuid): UUIDs (universally unique identifiers) genereren en valideren
* [hashicorp/go-multierror](https://pkg.go.dev/github.com/hashicorp/go-multierror): Met een enkele [`error`](https://pkg.go.dev/builtin?tab=doc#error) meerdere fouten tegelijk rapporteren
* [jmoiron/sqlx](https://pkg.go.dev/github.com/jmoiron/sqlx): Het inladen en sturen van data van en naar MySQL / MariaDB wat makkelijker maken (automatisch in structs laden etc.)
* [joho/godotenv](https://pkg.go.dev/github.com/joho/godotenv): .env-bestanden automatisch inladen bij het opstarten, maakt ontwikkeling makkelijker
* [rs/zerolog](https://pkg.go.dev/github.com/rs/zerolog): Efficiënt en gestructureerd loggen
* [spf13/pflag](https://pkg.go.dev/github.com/spf13/pflag): Een POSIX-compatibel alternatief voor het ingebouwde [flag](https://pkg.go.dev/flag)-pakket
* [spf13/viper](https://pkg.go.dev/github.com/spf13/viper): Een gecentraliseerde configuratiebron

## Opslag

Contrax gebruikt twee databases voor opslag.
De meest belangrijke is MySQL / MariaDB, waarin alle leningen en gebruikersinformatie wordt opgeslagen.
De tweede is Redis, hierin worden op het moment API keys opgeslagen die na een bepaalde hoeveelheid tijd
automatisch verlopen.

Er zijn twee 'wrappers', pakketten die de database aansturen, respectievelijk voor MySQL en Redis:
* [internal/database](internal/database)     (MySQL, generieke / algemene opslag)
* [internal/tokenstore](internal/tokenstore) (Redis, opslag van API tokens)